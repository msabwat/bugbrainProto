love.window.setTitle("Bug Brain")

img = love.graphics.newImage('bot.png')
imgScale = 1/20
 

local tile = 
   {
      w =  40,
      h =  40,
      color = {255, 255, 255}
   }

tile.__index = tile


function love.load()
   --load the bot image here
   w = love.graphics.getWidth()
   h = love.graphics.getHeight()
   --set img position
   x = w / 4
   y = h * (3/4)
   step = 50
   scale = 5 --platform scale
   
end


function tile:draw()
   love.graphics.setColor(self.color)
   love.graphics.rectangle("line", self.x, self.y, self.w, self.h)
end

function tile:bluedraw()
  
end
local tiles = {}


function love.keypressed(key)
   if key == "up" then y = y - step
   end
   if key == "down" then y = y + step
   end
   if key == "left" then x = x - step
   end
   if key == "right" then x = x + step
   end
   if key == "escape" then love.event.quit()
   end
end

function love.mousepressed(x, y, button, istouch)
   
   local tile = setmetatable({}, tile)
   tiles[#tiles + 1] = tile
   
      -- Basically do nothing
   tile.x = 10 + (100 * scale)+ 50
   tile.y = 410
   tile.color = {0,0,0}
   if ((x >= 10 and x <= 100 * scale) and (y >= 10 and y <= 80 * scale)) then       
      tile.x = x - (x % 40) 
      tile.y = y - (y % 40)
      if (tile.x == 0 or tile.y == 0) then
	 tile.x = 40
	 tile.y = 40
      end
   end
   if #tiles > 1 then
      if tile.x == tiles[#tiles - 1].x  and tile.y == tiles[#tiles - 1].y then
	 tile.color = {0, 0, -255}
      else
	 tile.color = tiles[#tiles - 1].color
      end
      --if previously drawn tile is the same, draw it in black then set color back to normal
   end
   if ((x >= 10 + (100 * scale)+ 50) and (x <= 10 + (100 * scale)+ 50 + 50))
   and ((y >= 10) and (y <= 10 + 50)) then
      tile.color = {255,0,0}
   end
   if ((x >= 10 + (100 * scale)+ 50) and (x <= 10 + (100 * scale)+ 50 + 50))
   and ((y >= 110) and (y <= 110 + 50)) then
      tile.color = {0,255,0}
   end
   if ((x >= 10 + (100 * scale)+ 50) and (x <= 10 + (100 * scale)+ 50 + 50))
   and ((y >= 210) and (y <= 210 + 50)) then
      tile.color = {0,0,255}
   end
   if ((x >= 10 + (100 * scale)+ 50) and (x <= 10 + (100 * scale)+ 50 + 50))
   and ((y >= 310) and (y <= 310 + 50)) then
      tile.color = {255,255,255}
   end
end	


function love.update(dt)
   
end

function draw_mapControls()
   love.graphics.setColor({255,0,0})
   love.graphics.rectangle("fill", 10 + (100 * scale)+ 50, 10,  50, 50)
   
   love.graphics.setColor({0,255,0})
   love.graphics.rectangle("fill", 10 + (100 * scale)+ 50, 10 + 100,  50, 50)
   
   love.graphics.setColor({0,0,255})
   love.graphics.rectangle("fill", 10 + (100 * scale)+ 50, 10 + 200,  50, 50) 
   
   love.graphics.setColor({255,255,255})
   love.graphics.rectangle("fill", 10 + (100 * scale)+ 50, 10 + 300,  50, 50)
   

end

function love.draw()
   love.graphics.setColor({255,255,255})
   love.graphics.rectangle("line", 10, 10, 100 * scale, 80 * scale)
   draw_mapControls();
   
   love.graphics.setColor({255,255,255})
   love.graphics.draw(img, x, y, 0, imgScale, imgScale)
   
   for i = 1, #tiles do
      tiles[i]:draw()
   end
   
   

   
end
